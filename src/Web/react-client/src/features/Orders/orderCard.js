import { Card, Badge } from "react-bootstrap";
import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { LinkContainer } from 'react-router-bootstrap'

import { selectOrderById } from "./orderSlice";

const OrderCard = ({orderId}) => {

    const order = useSelector(state => selectOrderById(state, orderId));
    const createdOn = new Date(order.createdOn);
    return(
        <>
        <Card>
            <LinkContainer to={`/order/${orderId}`}>
                <Card.Header as="h5" >
                    <Card.Title className="d-flex justify-content-between">
                        <div>{`Заказ от ${createdOn.toLocaleDateString()}`}</div>
                        <div>{`${order.price} ₽, ${order.isPaid ? "Оплачено": "Ожидает оплаты"}`}</div>
                    </Card.Title>
                    <Card.Subtitle>
                        <Link to={`/order/${orderId}`}>{order.code}</Link>
                    </Card.Subtitle>
                </Card.Header>
            </LinkContainer>

            <Card.Body>
                <Card.Title>
                    <h6>
                        Статус заказа <Badge variant="secondary">{order.orderState.state}</Badge>
                    </h6>
                </Card.Title>
            </Card.Body>

            
            <Card.Body>
                <Card.Title></Card.Title>
            </Card.Body>
        </Card>
        <br/>
        </>
    );
};

export default OrderCard;