import React, { Component } from 'react';
import {Nav} from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap'
import authService from './AuthorizeService';
import { ApplicationPaths } from './ApiAuthorizationConstants';

export class LoginMenu extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isAuthenticated: false,
            userName: null
        };
    }

    componentDidMount() {
        this._subscription = authService.subscribe(() => this.populateState());
        this.populateState();
    }

    componentWillUnmount() {
        authService.unsubscribe(this._subscription);
    }

    async populateState() {
        const [isAuthenticated, user] = await Promise.all([authService.isAuthenticated(), authService.getUser()])
        this.setState({
            isAuthenticated,
            userName: user && user.name
        });
    }

    render() {
        const { isAuthenticated, userName } = this.state;
        if (!isAuthenticated) {
            const registerPath = `${ApplicationPaths.Register}`;
            const loginPath = `${ApplicationPaths.Login}`;
            return this.anonymousView(registerPath, loginPath);
        } else {
            const profilePath = `${ApplicationPaths.Profile}`;
            const logoutPath = { pathname: `${ApplicationPaths.LogOut}`, state: { local: true } };
            return this.authenticatedView(userName, profilePath, logoutPath);
        }
    }

    authenticatedView(userName, profilePath, logoutPath) {
        return (
        <Nav className="mr-auto" variant="dark">
            <LinkContainer to={profilePath}>
                <Nav.Link>{userName}</Nav.Link>
            </LinkContainer>
            <LinkContainer to={logoutPath}>
                <Nav.Link>Выйти</Nav.Link>
            </LinkContainer>
        </Nav>);

    }

    anonymousView(registerPath, loginPath) {
        return (
        <Nav className="mr-auto" variant="dark">
            {/* <LinkContainer to={registerPath}>
                <Nav.Link>Зарегистрироваться</Nav.Link>
            </LinkContainer> */}

            <LinkContainer to={loginPath}>
                <Nav.Link>Войти или Зарегистрироваться</Nav.Link>
            </LinkContainer>
        </Nav>);
    }
}
