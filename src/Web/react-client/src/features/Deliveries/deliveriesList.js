import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import {Spinner } from "react-bootstrap";

import{
    fetchDeliveriesByOrder,
    selectDeliveriesIds
} from "./deliveriesSlice";
import DeliveryCard from './deliveryCard';

const DeliveriesList = ({orderId}) =>{

    const dispatch = useDispatch();

    const orderedDeliveriesIds = useSelector(selectDeliveriesIds);
    const error = useSelector((state) => state.deliveries.error);
    const [deliveriesRequestStatus, setDeliveriesRequestStatus] = useState("idle");
    const deliveriesStatus = useSelector(state => state.deliveries.status);

    useEffect(() =>{
        if(deliveriesRequestStatus === "idle" || deliveriesStatus === "idle"){
            dispatch(fetchDeliveriesByOrder(orderId));
            setDeliveriesRequestStatus("succeeded");
        }
    }, [deliveriesRequestStatus, deliveriesStatus, orderId, dispatch]);

    let content;

    if (deliveriesStatus === 'loading') {
        content = <Spinner animation="border" variant="primary" />
      } else if (deliveriesStatus === 'succeeded') {
        content = orderedDeliveriesIds.map((deliveryId) => (
          <DeliveryCard key={deliveryId} deliveryId ={deliveryId}/>
        ))
      } else if (deliveriesStatus === 'error') {
        content = <div>{error}</div>
      }

    return(
        <>{content}</>
    );
}

export default DeliveriesList;