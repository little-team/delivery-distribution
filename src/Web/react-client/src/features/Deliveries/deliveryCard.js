import { Card, Badge } from "react-bootstrap";
import { useSelector } from 'react-redux';

import {selectDeliveryById} from "./deliveriesSlice";

const DeliveryCard = ({deliveryId}) =>{

    const delivery = useSelector(state => selectDeliveryById(state, deliveryId));
    const createdOn = new Date(delivery.createdOn);

    return(
        <>
        <Card>
            <Card.Header>
                <Card.Subtitle>{`Доставка от ${createdOn.toLocaleDateString()}`}</Card.Subtitle>
            </Card.Header>
            
            <Card.Body>
                <p>
                    Статус доставки <Badge variant="secondary">{delivery.deliveryState.state}</Badge>
                </p>
            </Card.Body>
        </Card>
        <br/>
        </>
    );
};

export default DeliveryCard;