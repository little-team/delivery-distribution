﻿using System;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using FluentAssertions;
using Couriers.Core.Abstraction.Repositories;
using Couriers.Core.Domain;
using Couriers.Core.Services;
using Couriers.UnitTests.Attributes;
using Couriers.UnitTests.Builders.DomainModel.Domain;
using Moq;
using Xunit;
using Couriers.Core.Application.Exceptions;

namespace Couriers.UnitTests.DomainModel.Application.Services.CourierServiceTests
{
    public class GetCourierByIdAsyncTests
    {
        [Theory, AutoMoqData]
        public async Task GetCourierByIdAsync_CourierNotFound_ShouldThrowEntityNotFoundException(
            [Frozen] Mock<IRepository<Courier>> mockRepository,
            [Frozen] CourierService courierService
        )
        {
            //Arrange
            var courier = CourierBuilder.CreateBase();
            mockRepository.Setup(repo => repo.GetByIdAsync(courier.Id)).ReturnsAsync(default(Courier));
                
            //Act
            Func<Task<Courier>> act = async () => await courierService.GetCourierByIdAsync(courier.Id);

            //Assert
            await act.Should().ThrowAsync<EntityNotFoundException>();
        }

        [Theory, AutoMoqDataWithMapper]
        public async Task GetCourierByIdAsync_CourierFound_ShouldReturnFoundCourier(
            [Frozen] Mock<IRepository<Courier>> mockRepository,
            [Frozen] CourierService courierService
        )
        {
            //Arrange
            var courier = CourierBuilder.CreateBase();
            mockRepository.Setup(repo => repo.GetByIdAsync(courier.Id)).ReturnsAsync(courier);

            //Act
            var result = await courierService.GetCourierByIdAsync(courier.Id);

            //Assert
            result.Should().BeEquivalentTo(courier, opt => opt.ExcludingMissingMembers());
        }
    }
}