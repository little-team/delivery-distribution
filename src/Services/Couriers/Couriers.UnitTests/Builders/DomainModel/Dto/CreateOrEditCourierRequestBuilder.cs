﻿

using Couriers.WebHost.Dto.Courier;

namespace Couriers.UnitTests.Builders.DomainModel.Dto
{
    public static class CreateOrEditCourierRequestBuilder
    {
        public static CreateOrEditCourierRequest CreateBase()
        {
            return new CreateOrEditCourierRequest()
            {
                Name = "Name",
                PhoneNumber = "12345"
            };
        }
    }
}