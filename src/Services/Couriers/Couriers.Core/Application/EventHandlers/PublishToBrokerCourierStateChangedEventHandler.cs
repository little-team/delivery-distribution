﻿using System.Threading;
using System.Threading.Tasks;
using Couriers.Core.Application.Events;
using DeliveryDistribution.Integration.Contracts;
using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Deliveries.Core.Application.EventHandlers
{
    public class PublishToBrokerCourierStateChangedEventHandler:
        INotificationHandler<CourierStateUpdatedEvent>
    {
        private readonly IPublishEndpoint _publishEndpoint;
        private readonly ILogger<PublishToBrokerCourierStateChangedEventHandler> _logger;

        public PublishToBrokerCourierStateChangedEventHandler(
            IPublishEndpoint publishEndpoint, 
            ILogger<PublishToBrokerCourierStateChangedEventHandler> logger)
        {
            _publishEndpoint = publishEndpoint;
            _logger = logger;
        }

        public async Task Handle(CourierStateUpdatedEvent notification, CancellationToken cancellationToken)
        {
            _logger.LogInformation("{@CourierStateUpdatedEvent} is publishing to broker", notification );
            await _publishEndpoint.Publish<CourierStateChanged>(new
            {
                notification.CourierId,
                notification.CourierStateId
            }, 
                cancellationToken);
            _logger.LogInformation("{@CourierStateUpdatedEvent} was published to broker", notification );
        }
    }
}