using System;

namespace Couriers.Core.Abstraction
{
    public interface ICurrentDateTimeProvider
    {
        DateTime CurrentDateTime { get; }
    }
}