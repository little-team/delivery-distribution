﻿

namespace Couriers.Core.Domain.Enumerations
{
    public class CourierState : Enumeration
    {
        public static CourierState Undefined = new CourierState(0, nameof(Undefined));
        public static CourierState Active = new CourierState(1, nameof(Active));
        public static CourierState Dismissed = new CourierState(2, nameof(Dismissed));
        public static CourierState DayOff = new CourierState(3, nameof(DayOff));
        public static CourierState Vacation = new CourierState(4, nameof(Vacation));
        
        public CourierState(int id, string name) : base(id, name)
        {
        }
    }
}