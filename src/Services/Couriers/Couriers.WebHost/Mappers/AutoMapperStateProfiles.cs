﻿using AutoMapper;
using Couriers.Core.Domain.Enumerations;
using Couriers.WebHost.Dto.States;

namespace Couriers.WebHost.Mappers
{
    public class AutoMapperStateProfiles: Profile
    {
        public AutoMapperStateProfiles()
        {

            CreateMap<CourierState, StateResponse>()
                .ForMember(dst => dst.State, 
                    opt => opt.MapFrom(src =>
                        src.Name));
        }
    }
}