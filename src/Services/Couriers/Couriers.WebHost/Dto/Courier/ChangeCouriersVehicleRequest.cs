﻿using System;

namespace Couriers.WebHost.Dto.Courier
{
    public record ChangeCouriersVehicleRequest
    {
        public Guid VehicleId { get; set; }
    }
}