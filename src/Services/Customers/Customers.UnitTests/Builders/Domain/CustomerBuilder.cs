﻿using System;
using System.Collections.Generic;
using Customers.Core.Domain;

namespace Customers.UnitTests.Builders.Domain
{
    public static class CustomerBuilder
    {
        public static Customer CreateBase()
        {
            return new Customer()
            {
                Id = Guid.Parse("8563436e-86bf-4d1f-aa13-372299c2a526"),
                Name = "Test Customer Name",
                PhoneNumber = "6666666666",
                Email = "customer.test@test.com"
            };
        }

        public static Customer WithEmptyId(this Customer customer)
        {
            customer.Id = Guid.Empty;
            return customer;
        }

        public static Customer WithName(this Customer customer, string name)
        {
            customer.Name = name;
            return customer;
        }

        public static Customer WithPhone(this Customer customer, string phone)
        {
            customer.PhoneNumber = phone;
            return customer;
        }
        public static Customer WithEmail(this Customer customer, string email)
        {
            customer.Email = email;
            return customer;
        }
    }
}