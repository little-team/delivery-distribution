﻿using System;
using System.Collections.Generic;

namespace Customers.WebHost.Dto.Customer
{
    public record CustomerDetailedResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
    }
}
