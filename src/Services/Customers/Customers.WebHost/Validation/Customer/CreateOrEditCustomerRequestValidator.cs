﻿using Customers.WebHost.Dto.Customer;
using FluentValidation;

namespace Customers.WebHost.Validation.Customer
{
    public class CreateOrEditCustomerRequestValidator : AbstractValidator<CreateOrEditCustomerRequest>
    {
        public CreateOrEditCustomerRequestValidator()
        {
            this.RuleFor(request => request.Name).NotEmpty().Length(5, 50);
            //проверяем, что количество символов совпадает со стандартной длиной номера +7... или 8...
            this.RuleFor(request => request.PhoneNumber).NotEmpty().Length(11, 12);
            this.RuleFor(request => request.Email).EmailAddress();
        }
    }
}
