namespace Customers.DataAccess.Data.DbInitializer
{
    public interface IDbInitializer
    {
        public void InitializeDb();
    }
}