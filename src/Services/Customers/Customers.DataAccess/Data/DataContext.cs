using Customers.Core.Domain;
using Microsoft.EntityFrameworkCore;

namespace Customers.DataAccess.Data
{
    public class DataContext: DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        
        public DataContext()
        {
            
        }
        
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}