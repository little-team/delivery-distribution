﻿using System.Threading.Tasks;
using AutoFixture.Xunit2;
using FluentAssertions;
using Orders.Core.Application.Strategies;
using Orders.UnitTests.Attributes;
using Xunit;

namespace Orders.UnitTests.Core.Application.Strategies.ConstantComputeOrderPriceStrategyTests
{
    public class ComputePriceAsyncTests
    {
        [Theory, AutoMoqData]
        public async Task ComputePriceAsync_CorrectInput_Return42(
            [Frozen] ConstantComputeOrderPriceStrategy sut)
        {
            //arrange
            var weight = 10;
            var departureAddress = "departure";
            var destinationAddress = "destination";
            
            //act
            var result = await sut.ComputePriceAsync(weight, departureAddress, destinationAddress);
            
            //assert
            result.Should().Be(42);
        }
    }
}