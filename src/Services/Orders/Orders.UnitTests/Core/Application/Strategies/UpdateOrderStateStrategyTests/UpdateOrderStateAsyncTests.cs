﻿using System.Threading.Tasks;
using AutoFixture.Xunit2;
using Moq;
using Orders.Core.Abstraction.Services;
using Orders.Core.Application.Strategies;
using Orders.UnitTests.Attributes;
using Orders.UnitTests.Builders.Domain;
using Xunit;

namespace Orders.UnitTests.Core.Application.Strategies.UpdateOrderStateStrategyTests
{
    public class UpdateOrderStateAsyncTests
    {
        [Theory, AutoMoqData]
        public async Task UpdateOrderStateAsync_DeliveryStateIdIs0_EditOrderStatusAsyncNotInvoked(
            [Frozen] Mock<IOrderService> mockOrderService,
            [Frozen] UpdateOrderStateStrategy sut)
        {
            //arrange
            var orderId = OrderBuilder.CreateBase().Id;
            var deliveryStateId = 0;
            
            //act
            await sut.UpdateOrderStateAsync(orderId, deliveryStateId);
            
            //assert
            mockOrderService.Verify(service => service.EditOrderStatusAsync(orderId, It.IsAny<int>()), Times.Never);
        }

        [Theory, AutoMoqData]
        public async Task UpdateOrderStateAsync_DeliveryStateis1_EditOrderStatusAsyncInvoked(
            [Frozen] Mock<IOrderService> mockOrderService,
            [Frozen] UpdateOrderStateStrategy sut)
        {
            //arrange
            var orderId = OrderBuilder.CreateBase().Id;
            var deliveryStateId = 1;
            
            //act
            await sut.UpdateOrderStateAsync(orderId, deliveryStateId);
            
            //assert
            mockOrderService.Verify(service => service.EditOrderStatusAsync(orderId, 2),Times.Once);
        }
    }
}