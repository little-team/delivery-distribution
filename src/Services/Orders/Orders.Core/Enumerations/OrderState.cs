namespace Orders.Core.Enumerations
{
    public class OrderState: Enumeration
    {
        public static OrderState Undefined = new OrderState(0, nameof(Undefined));
        
        public static OrderState New = new OrderState(1, "Новый");

        public static OrderState Processing = new OrderState(2, "В работе");

        public static OrderState InDelivery = new OrderState(3, "Доставляется");

        public static OrderState Delivered = new OrderState(4, "Доставлен");

        public static OrderState PartialDelivered = new OrderState(5, "Частично доставлен");

        public static OrderState Canceled = new OrderState(6, "Отменён");
        
        public OrderState(int id, string name) : base(id, name)
        {
            
        }
    }
}