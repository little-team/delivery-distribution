﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Orders.Core.Abstraction;
using Orders.Core.Abstraction.Repositories;
using Orders.Core.Abstraction.Services;
using Orders.Core.Abstraction.Strategies;
using Orders.Core.Application.Events;
using Orders.Core.Application.Exceptions;
using Orders.Core.Domain;
using Orders.Core.Enumerations;
using Orders.Core.Other;

namespace Orders.Core.Application.Services
{
    public class OrderService : IOrderService
    {
        private readonly IRepository<Order> _orderRepository;
        private readonly ILogger<OrderService> _logger;
        private readonly IMediator _mediator;

        private readonly IOptions<OperationSettings> _options;
        
        private readonly ICurrentDateTimeProvider _currentDateTimeProvider;

        private readonly IComputeOrderPriceStrategy _computeOrderPriceStrategy;

        public OrderService(
            IRepository<Order> orderRepository,
            IOptions<OperationSettings> configurationSettingsAccessor,
            ICurrentDateTimeProvider currentDateTimeProvider,
            ILogger<OrderService> logger, 
            IMediator mediator, IComputeOrderPriceStrategy computeOrderPriceStrategy)
        {
            _orderRepository = orderRepository;
            _logger = logger;
            _mediator = mediator;
            _computeOrderPriceStrategy = computeOrderPriceStrategy;
            //Тут оставляем весь объект IOptions,т.к. если брать только value,
            //то тесты не сработают
            _options = configurationSettingsAccessor;        
            _currentDateTimeProvider = currentDateTimeProvider;
        }

        public async Task<IEnumerable<Order>> GetOrdersAsync(Guid? customerId = null)
        {
            IEnumerable<Order> orders;

            if (customerId.HasValue)
            {
                orders = await _orderRepository.GetOnConditionAsync(x => x.CustomerId == customerId);
            }
            else
            {
                orders = await _orderRepository.GetAllAsync();
            }
            
            _logger.LogInformation("Found order list: {@Orders}", orders);

            return orders;
        }

        public async Task<Order> GetOrderByIdAsync(Guid id)
        {
            var order = await _orderRepository.GetByIdAsync(id);
            if (order == null)
            {
                _logger.LogWarning("Order with id: {@OrderId} not found", id);
                throw new EntityNotFoundException(id);
            }
            
            _logger.LogInformation("Found order: {@Order}", order);

            return order;
        }

        
        public async Task<Guid> AddOrderAsync(Order newOrder)
        {
            newOrder.CreatedOn = _currentDateTimeProvider.CurrentDateTime;
            newOrder.UpdatedOn = _currentDateTimeProvider.CurrentDateTime;
            newOrder.OrderStateId = OrderState.New.Id;
            // Get new order code
            var orders = await _orderRepository.GetAllAsync();
            newOrder.Code = GetNewOrderCode(orders.ToList());
            //add order to db 
            _logger.LogInformation("Adding order: {@Order}", newOrder);
            await _orderRepository.AddAsync(newOrder);
            _logger.LogInformation("Added order: {@Order}", newOrder);
            
            await _mediator.Publish(new OrderCreatedEvent()
            {
                OrderId = newOrder.Id,
                Weight = newOrder.Weight
            });
            await _mediator.Publish(new OrderStateUpdatedEvent()
            {
                OrderId = newOrder.Id,
                OrderStateId = newOrder.OrderStateId
            });
            
            return newOrder.Id;

        }
        private string GetNewOrderCode(List<Order> ordersList)
        {
            if (ordersList.All(o => o.Code == null) 
                || !ordersList.Any(o => o.Code.StartsWith(_options.Value.OrderSettings.OrderPrefix)))
            {
                return Tools.GetInitialOrderCode(_options.Value.OrderSettings);                
            }

            return Tools.GetNextOrderCode(
                ordersList.Where(s => 
                    s.Code.StartsWith(_options.Value.OrderSettings.OrderPrefix)).Max(r => r.Code),
                _options.Value.OrderSettings);


        }
        
        public async Task EditOrderAsync(Guid id, Order updatedOrder)
        {
            var order = await _orderRepository.GetByIdAsync(id);
            if (order == null)
            {
                _logger.LogWarning("Order with id: {@OrderId} not found", id);
                throw new EntityNotFoundException(id);
            }
            _logger.LogInformation("Updating order: {@Order}", order);

            order.DepartureAddress = updatedOrder.DepartureAddress;
            order.DestinationAddress = updatedOrder.DestinationAddress;
            order.Weight = updatedOrder.Weight;
            order.Price = updatedOrder.Price;
            order.UpdatedOn = _currentDateTimeProvider.CurrentDateTime;

            await _orderRepository.UpdateAsync(order);
            _logger.LogInformation("Updated order: {@Order}", order);
        }

        public async Task DeleteOrderAsync(Guid id)
        {
            var order = await _orderRepository.GetByIdAsync(id);
            if (order == null)
            {
                _logger.LogWarning("Order with id: {@OrderId} not found", id);
                throw new EntityNotFoundException(id);
            }
            
            _logger.LogInformation("Deleting order: {@Order}", order);
            await _orderRepository.DeleteAsync(order);
            _logger.LogInformation("Deleted order: {@Order}", order);
        }

        public async Task EditOrderStatusAsync(Guid id, int newStateId)
        {
            var order = await _orderRepository.GetByIdAsync(id);
            if (order == null)
            {
                _logger.LogWarning("Order with id: {@OrderId} not found", id);
                throw new EntityNotFoundException(id);
            }

            if (order.OrderStateId == newStateId)
            {
                _logger.LogInformation("Order already has state: {@NewStateId}", newStateId);
                return;
            }
            
            order.UpdatedOn = _currentDateTimeProvider.CurrentDateTime;
            order.OrderStateId = newStateId;
            await _orderRepository.UpdateAsync(order);
            _logger.LogInformation("Updating order state: {@Order}", order);

            await _mediator.Publish(new OrderStateUpdatedEvent()
            {
                OrderId = order.Id,
                OrderStateId = order.OrderStateId
            });
        }

        public async Task<double> ComputePrice(double weight, string departureAddress, string destinationAddress)
        {
            return await _computeOrderPriceStrategy.ComputePriceAsync(weight, departureAddress, destinationAddress);
        }

        public async Task SetPaidByIdAsync(Guid id)
        {
            var order = await _orderRepository.GetByIdAsync(id);
            if (order == null)
            {
                _logger.LogWarning("Order with id: {@OrderId} not found", id);
                throw new EntityNotFoundException(id);
            }

            order.IsPaid = true;
            order.UpdatedOn = _currentDateTimeProvider.CurrentDateTime;
            await _orderRepository.UpdateAsync(order);
        }
    }
}