﻿using System.Threading;
using System.Threading.Tasks;
using DeliveryDistribution.Integration.Contracts;
using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using Orders.Core.Application.Events;

namespace Orders.Core.Application.EventHandlers
{
    public class PublishToBrokerWhenOrderCreatedEventHandler:INotificationHandler<OrderCreatedEvent>
    {
        private readonly IPublishEndpoint _publishEndpoint;
        private readonly ILogger<PublishToBrokerWhenOrderCreatedEventHandler> _logger;
        public PublishToBrokerWhenOrderCreatedEventHandler(
            IPublishEndpoint publishEndpoint,
            ILogger<PublishToBrokerWhenOrderCreatedEventHandler> logger)
        {
            _publishEndpoint = publishEndpoint;
            _logger = logger;
        }

        public async Task Handle(OrderCreatedEvent notification, CancellationToken cancellationToken)
        {
            _logger.LogInformation("{@OrderCreatedEvent} is publishing to broker", notification );
            await _publishEndpoint.Publish<OrderCreated>(new
            {
                OrderId = notification.OrderId,
                Weight = notification.Weight
            }, cancellationToken);
            _logger.LogInformation("{@OrderCreatedEvent} was published to broker", notification );
        }
    }
}