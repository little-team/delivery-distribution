﻿using System.Threading;
using System.Threading.Tasks;
using DeliveryDistribution.Integration.Contracts;
using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using Orders.Core.Application.Events;

namespace Orders.Core.Application.EventHandlers
{
    public class PublishToBrokerWhenOrderStateUpdatedEventHandler
        :INotificationHandler<OrderStateUpdatedEvent>
    {
        private readonly IPublishEndpoint _publishEndpoint;
        private readonly ILogger<PublishToBrokerWhenOrderStateUpdatedEventHandler> _logger;

        public PublishToBrokerWhenOrderStateUpdatedEventHandler(
            IPublishEndpoint publishEndpoint, 
            ILogger<PublishToBrokerWhenOrderStateUpdatedEventHandler> logger)
        {
            _publishEndpoint = publishEndpoint;
            _logger = logger;
        }
        
        public async Task Handle(OrderStateUpdatedEvent notification, CancellationToken cancellationToken)
        {
            _logger.LogInformation("{@OrderStateUpdatedEvent} is publishing to broker", notification );
            await _publishEndpoint.Publish<OrderStateUpdated>(new
            {
                OrderId = notification.OrderId,
                OrderStateId = notification.OrderStateId
            }, cancellationToken);
            _logger.LogInformation("{@OrderStateUpdatedEvent} was published to broker", notification );
        }
    }
}