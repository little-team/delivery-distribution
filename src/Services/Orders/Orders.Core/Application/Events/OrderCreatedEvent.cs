﻿using System;
using MediatR;

namespace Orders.Core.Application.Events
{
    public class OrderCreatedEvent:INotification
    {
        public Guid OrderId { get; set; }
        
        public double Weight { get; set; }
    }
}