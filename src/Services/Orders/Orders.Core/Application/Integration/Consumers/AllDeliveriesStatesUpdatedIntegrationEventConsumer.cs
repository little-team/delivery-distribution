﻿using System.Threading.Tasks;
using DeliveryDistribution.Integration.Contracts;
using MassTransit;
using Orders.Core.Abstraction.Strategies;

namespace Orders.Core.Application.Integration.Consumers
{
    public class AllDeliveriesStatesUpdatedIntegrationEventConsumer
        :IConsumer<AllDeliveriesStatesUpdatedIntegrationEvent>
    {
        
        private readonly IUpdateOrderStateStrategy _updateOrderStateStrategy;

        public AllDeliveriesStatesUpdatedIntegrationEventConsumer(
            IUpdateOrderStateStrategy updateOrderStateStrategy)
        {
            _updateOrderStateStrategy = updateOrderStateStrategy;
        }

        public async Task Consume(ConsumeContext<AllDeliveriesStatesUpdatedIntegrationEvent> context)
        {
            var message = context.Message;
            await _updateOrderStateStrategy.UpdateOrderStateAsync(
                message.OrderId, message.DeliveryStateId);
        }
    }
}