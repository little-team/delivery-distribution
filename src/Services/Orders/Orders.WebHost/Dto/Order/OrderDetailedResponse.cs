﻿using System;

namespace Orders.WebHost.Dto.Order
{
    public record OrderDetailedResponse
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string DepartureAddress { get; set; }
        public string DestinationAddress { get; set; }
        public double Weight { get; set; }
        public double Price { get; set; }        
        public StateResponse OrderState { get; set; }
        public Guid CustomerId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public bool IsPaid { get; set; }
    }
}