﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Orders.Core.Abstraction.Services;
using Orders.Core.Domain;
using Orders.WebHost.Dto.Order;

namespace Orders.WebHost.Controllers
{
    /// <summary>
    /// Контроллер заказов
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderService _orderService;
        private readonly IOrderStateHistoryService _orderStateHistoryService;
        private readonly IMapper _mapper;

        public OrdersController(
            IOrderService orderService,
            IMapper mapper, 
            IOrderStateHistoryService orderStateHistoryService)
        {
            _orderService = orderService;
            _mapper = mapper;
            _orderStateHistoryService = orderStateHistoryService;
        }

        /// <summary>
        /// Получить список заказов
        /// </summary>
        /// <param name="customerId">Id пользователя</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<OrderShortResponse>>> GetOrderAsync(Guid? customerId)
        {
            var orders = await _orderService.GetOrdersAsync(customerId);
            var response = _mapper.Map<IEnumerable<OrderShortResponse>>(orders);
            return Ok(response);

        }

        /// <summary>
        /// Получить данные заказа по идентификатору
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}", Name = "GetOrderAsync")]
        public async Task<ActionResult<OrderDetailedResponse>> GetOrderByIdAsync(Guid id)
        {
            var order = await _orderService.GetOrderByIdAsync(id);
            var response = _mapper.Map<OrderDetailedResponse>(order);
            return Ok(response);
        }

        /// <summary>
        /// Создать новый заказ
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> AddOrderAsync(CreateOrEditOrderRequest request)
        {
            var newOrder = _mapper.Map<Order>(request);
            var responseId = await _orderService.AddOrderAsync(newOrder);
            return CreatedAtRoute(nameof(GetOrderAsync), new {id = responseId}, responseId);
            
        }

        /// <summary>
        /// Изменить данные заказа по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditOrderAsync(Guid id, CreateOrEditOrderRequest request)
        {
            var updatedOrder = new Order();
            _mapper.Map(request, updatedOrder);
            await _orderService.EditOrderAsync(id, updatedOrder);
            return Ok();
        }


        /// <summary>
        /// Удалить заказ 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteOrderAsync(Guid id)
        {
            await _orderService.DeleteOrderAsync(id);
            return Ok();
        }        
        
        
        /// <summary>
        /// Изменить статус заказа по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>        
        [HttpPost("{id:guid}/SetStatus")]
        public async Task<IActionResult> EditOrderStatusAsync(Guid id, EditOrderStatusRequest request)
        {
            var newStateId = request.State;
            await _orderService.EditOrderStatusAsync(id, newStateId);
            return Ok();
        }


        /// <summary>
        /// Вычисление стоимости заказа
        /// </summary>
        /// <param name="weight">Вес заказа</param>
        /// <param name="departureAddress">Адрес отправления</param>
        /// <param name="destinationAddress">Адрес назначения</param>
        /// <returns></returns>
        [HttpGet("Compute-price")]
        public async Task<ActionResult<double>> ComputeOrderPriceAsync(double weight, string departureAddress, string destinationAddress)
        {
            var price = await _orderService.ComputePrice(weight, departureAddress,
                destinationAddress);
            return Ok(price);
        }
        
        /// <summary>
        /// Получить историю изменений статусов для выбранного заказа
        /// </summary>
        /// <param name="id">id заказа</param>
        /// <returns></returns>
        [HttpGet("{id:guid}/history")]
        public async Task<ActionResult<IEnumerable<OrderStateHistoryResponse>>> GetHistoryForDeliveryByIdAsync(
            Guid id)
        {
            var historyList = await _orderStateHistoryService.GetHistoryForOrderByIdAsync(id);
            var response = _mapper.Map<IEnumerable<OrderStateHistoryResponse>>(historyList);
            return Ok(response);
        }

        /// <summary>
        /// Отметить заказ оплаченным
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("{id:guid}/setPaid")]
        public async Task<ActionResult> SetOrderPaidByIdAsync(Guid id)
        {
            await _orderService.SetPaidByIdAsync(id);
            return Ok();
        }
    }
}