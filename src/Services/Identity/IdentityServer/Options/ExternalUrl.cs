﻿namespace IdentityServer.Options
{
    public class ExternalUrl
    {
        public string SpaHostUrl { get; set; }
        
        public string OrdersApiUrl { get; set; }
        
        public string DeliveriesApiUrl { get; set; }
        
        public string CouriersApiUrl { get; set; }
        
        public string CustomersApiUrl { get; set; }
        
        public string Authority { get; set; }
    }
}