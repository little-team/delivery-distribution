using System;

namespace Deliveries.Core.Domain
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}