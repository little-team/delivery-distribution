﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Deliveries.Core.Abstraction;
using Deliveries.Core.Abstraction.Repositories;
using Deliveries.Core.Abstraction.Services;
using Deliveries.Core.Domain;

namespace Deliveries.Core.Application.Services
{
    public class DeliveryStateHistoryService: IDeliveryStateHistoryService
    {

        private readonly IRepository<DeliveryStateHistory> _deliveryStateHistoryRepository;
        private readonly ICurrentDateTimeProvider _currentDateTimeProvider;

        public DeliveryStateHistoryService(
            IRepository<DeliveryStateHistory> deliveryStateHistoryRepository, 
            ICurrentDateTimeProvider currentDateTimeProvider)
        {
            _deliveryStateHistoryRepository = deliveryStateHistoryRepository;
            _currentDateTimeProvider = currentDateTimeProvider;
        }

        public async Task AddDeliveryStateAsync(DeliveryStateHistory newDeliveryStateHistory)
        {
            newDeliveryStateHistory.UpdatedOn = _currentDateTimeProvider.CurrentDateTime;
            await _deliveryStateHistoryRepository.AddAsync(newDeliveryStateHistory);
        }

        public async Task<IEnumerable<DeliveryStateHistory>> GetHistoryForDeliveryByIdAsync(Guid deliveryId)
        {
            var historyList = await _deliveryStateHistoryRepository
                .GetOnConditionAsync(item => item.DeliveryId == deliveryId);
            return historyList;
        }
    }
}