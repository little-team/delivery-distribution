﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Deliveries.Core.Abstraction;
using Deliveries.Core.Abstraction.Repositories;
using Deliveries.Core.Abstraction.Services;
using Deliveries.Core.Application.Events;
using Deliveries.Core.Application.Exceptions;
using Deliveries.Core.Domain;
using Deliveries.Core.Enumerations;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Deliveries.Core.Application.Services
{
    public class DeliveryService : IDeliveryService
    {
        private readonly IRepository<Delivery> _deliveryRepository;
        private readonly ICurrentDateTimeProvider _currentDateTimeProvider;
        private readonly ILogger<DeliveryService> _logger;
        private readonly IMediator _mediator;

        public DeliveryService(
            IRepository<Delivery> deliveryRepository,
            ICurrentDateTimeProvider currentDateTimeProvider,
            ILogger<DeliveryService> logger,
            IMediator mediator)
        {
            _deliveryRepository = deliveryRepository;
            _currentDateTimeProvider = currentDateTimeProvider;
            _logger = logger;
            _mediator = mediator;
        }

        public async Task<IEnumerable<Delivery>> GetDeliveriesAsync()
        {
            var deliveries = await _deliveryRepository.GetAllAsync();
            return deliveries;
        }

        public async Task<Delivery> GetDeliveryByIdAsync(Guid id)
        {
            var delivery = await _deliveryRepository.GetByIdAsync(id);
            if (delivery == null)
            {
                _logger.LogWarning("Delivery with id: {@DeliveryId} not found", id);
                throw new EntityNotFoundException(id);
            }
            
            _logger.LogInformation("Found delivery: {@Delivery}", delivery);
            return delivery;
        }
        
        
        public async Task<Guid> AddDeliveryAsync(Delivery newDelivery)
        {
            
            newDelivery.CreatedOn = _currentDateTimeProvider.CurrentDateTime;
            newDelivery.UpdatedOn = _currentDateTimeProvider.CurrentDateTime;
            newDelivery.DeliveryStateId = DeliveryState.Processing.Id;
            
            var guid  = await _deliveryRepository.AddAsync(newDelivery);
            _logger.LogInformation("Added delivery: {@Delivery}", newDelivery);
            
            await _mediator.Publish(new DeliveryStateUpdatedEvent()
            {
                DeliveryId = newDelivery.Id,
                DeliveryStateId = newDelivery.DeliveryStateId
            });
            
            return guid;
        }
        
        public async Task<IEnumerable<Delivery>> GetCourierDeliveriesAsync(Guid? id)
        {
            var deliveries = await _deliveryRepository.GetOnConditionAsync(x => x.CourierId == id);
            return deliveries;
        }

        public async Task<IEnumerable<Delivery>> GetOrderDeliveriesAsync(Guid id)
        {
            var deliveries = await _deliveryRepository.GetOnConditionAsync(x => x.OrderId == id);
            return deliveries;
        }

        public async Task EditDeliveryAsync(Guid id, Delivery editedDelivery)
        {
            var delivery = await _deliveryRepository.GetByIdAsync(id);
            if (delivery == null)
            {
                _logger.LogWarning("Delivery with id: {@DeliveryId} not found", id);
                throw new EntityNotFoundException(id);
            }

            var oldDeliveryStateId = delivery.DeliveryStateId;
            delivery.CourierId = editedDelivery.CourierId;
            delivery.UpdatedOn = _currentDateTimeProvider.CurrentDateTime;
            delivery.DeliveryStateId = editedDelivery.DeliveryStateId;
            
            await _deliveryRepository.UpdateAsync(delivery);
            _logger.LogInformation("Updated delivery: {@Delivery}", delivery);
            
            //Если статус доставки изменился, проверям статусы всех доставок данного заказа
            if (delivery.DeliveryStateId != oldDeliveryStateId)
            {
                await _mediator.Publish(new DeliveryStateUpdatedEvent()
                {
                    DeliveryId = delivery.Id,
                    DeliveryStateId = delivery.DeliveryStateId
                });
                
                var orderDeliveries = await _deliveryRepository.GetOnConditionAsync(
                    item => item.OrderId == delivery.OrderId);
                
                var allTheSame = true;
                foreach (var item in orderDeliveries)
                {
                    if (item.DeliveryStateId != editedDelivery.DeliveryStateId)
                    {
                        allTheSame = false;
                        break;
                    }
                }
                //Если у всех одинаковый статус, то бросаем событие, чтобы у Заказа тоже обновился статус
                if (allTheSame)
                {
                    await _mediator.Publish(new AllDeliveriesStatesForOrderUpdatedEvent()
                    {
                        OrderId = delivery.OrderId,
                        DeliveryStateId = delivery.DeliveryStateId,
                    });
                }
            }
        }

        public async Task UpdateDeliveryStateByIdAsync(Guid id, int stateId)
        {
            var delivery = await _deliveryRepository.GetByIdAsync(id);
            
            if (delivery == null)
            {
                _logger.LogWarning("Delivery with id: {@DeliveryId} not found", id);
                throw new EntityNotFoundException(id);
            }

            var oldDeliveryStateId = delivery.DeliveryStateId;

            delivery.DeliveryStateId = stateId;
            delivery.UpdatedOn = _currentDateTimeProvider.CurrentDateTime;
            
            await _deliveryRepository.UpdateAsync(delivery);
            
            //Если статус доставки изменился, проверям статусы всех доставок данного заказа
            if (delivery.DeliveryStateId != oldDeliveryStateId)
            {
                //Публикуем событие, что изменился статус доставки
                await _mediator.Publish(new DeliveryStateUpdatedEvent()
                {
                    DeliveryId = delivery.Id,
                    DeliveryStateId = delivery.DeliveryStateId
                });
                
                //Проверям статусы остальных доставок этого же заказа
                var orderDeliveries = await _deliveryRepository.GetOnConditionAsync(
                    item => item.OrderId == delivery.OrderId);
                
                var allTheSame = true;
                foreach (var item in orderDeliveries)
                {
                    if (item.DeliveryStateId != delivery.DeliveryStateId)
                    {
                        allTheSame = false;
                        break;
                    }
                }
                //Если у всех одинаковый статус, то бросаем событие, чтобы у Заказа тоже обновился статус
                if (allTheSame)
                {
                    await _mediator.Publish(new AllDeliveriesStatesForOrderUpdatedEvent()
                    {
                        OrderId = delivery.OrderId,
                        DeliveryStateId = delivery.DeliveryStateId,
                    });
                }
            }
        }

        public async Task UpdateDeliveryCourierByIdAsync(Guid id, Guid courierId)
        {
            var delivery = await _deliveryRepository.GetByIdAsync(id);
            
            if (delivery == null)
            {
                _logger.LogWarning("Delivery with id: {@DeliveryId} not found", id);
                throw new EntityNotFoundException(id);
            }

            delivery.CourierId = courierId;
            delivery.UpdatedOn = _currentDateTimeProvider.CurrentDateTime;
            
            await _deliveryRepository.UpdateAsync(delivery);
        }

        public async Task DeleteDeliveryByIdAsync(Guid id)
        {
            var delivery = await _deliveryRepository.GetByIdAsync(id);
            if (delivery == null)
            {
                _logger.LogWarning("Delivery with id: {@DeliveryId} not found", id);
                throw new EntityNotFoundException(id);
            }
            
            await _deliveryRepository.DeleteAsync(delivery);
            _logger.LogInformation("Deleted delivery: {@Delivery}", delivery);
        }
    }
}
