using System;
using Deliveries.Core.Abstraction;

namespace Deliveries.Core.Application
{
    public class CurrentDateTimeProvider:ICurrentDateTimeProvider
    {
        public DateTime CurrentDateTime => DateTime.Now;
    }
}