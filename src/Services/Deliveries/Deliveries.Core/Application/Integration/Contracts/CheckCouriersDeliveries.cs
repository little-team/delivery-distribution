using System;

// ReSharper disable once CheckNamespace
namespace DeliveryDistribution.Integration.Contracts
{
    // ReSharper disable once InconsistentNaming
    public interface CheckCouriersDeliveries
    {
        Guid CourierId { get; set; }
    }
    
    // ReSharper disable once InconsistentNaming
    public interface CheckCouriersDeliveriesResult
    {
        bool Result { get; set; }
    }
}