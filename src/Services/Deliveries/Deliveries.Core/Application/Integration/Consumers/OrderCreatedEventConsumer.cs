﻿using System.Threading.Tasks;
using Deliveries.Core.Abstraction.Strategies;
using DeliveryDistribution.Integration.Contracts;
using MassTransit;
using Microsoft.Extensions.Logging;

namespace Deliveries.Core.Application.Integration.Consumers
{
    public class OrderCreatedEventConsumer:IConsumer<OrderCreated>
    {
        private readonly ICreateDeliveriesStrategy _createDeliveriesStrategy;
        private ILogger<OrderCreatedEventConsumer> _logger;
        
        public OrderCreatedEventConsumer(
            ICreateDeliveriesStrategy createDeliveriesStrategy,
            ILogger<OrderCreatedEventConsumer> logger)
        {
            _createDeliveriesStrategy = createDeliveriesStrategy;
            _logger = logger;
        }
        
        public async Task Consume(ConsumeContext<OrderCreated> context)
        {
            var message = context.Message;
            _logger.LogInformation("{@OrderCreated} message consumed from broker", message);
            await _createDeliveriesStrategy.CreateAsync(message.OrderId, message.Weight);
        }
    }
}