﻿using System.Linq;
using System.Threading.Tasks;
using Deliveries.Core.Abstraction.Services;
using DeliveryDistribution.Integration.Contracts;
using MassTransit;

namespace Deliveries.Core.Application.Integration.Consumers
{
    public class CheckCouriersDeliveriesConsumer: IConsumer<CheckCouriersDeliveries>
    {
        private readonly IDeliveryService _deliveryService;

        public CheckCouriersDeliveriesConsumer(
            IDeliveryService deliveryService)
        {
            _deliveryService = deliveryService;
        }

        public async Task Consume(ConsumeContext<CheckCouriersDeliveries> context)
        {
            var deliveries = await _deliveryService.GetCourierDeliveriesAsync(context.Message.CourierId);
            await context.RespondAsync<CheckCouriersDeliveriesResult>(new
            {
                Result = deliveries.Any()
            });
        }
    }
}