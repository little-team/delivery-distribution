using System.Linq;
using Deliveries.Core.Enumerations;
using Microsoft.EntityFrameworkCore;

namespace Deliveries.DataAccess.Data.DbInitializer
{
    public class EfDbInitializer : IDbInitializer
    {

        private readonly DataContext _dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            _dataContext.Database.Migrate();

            if (!_dataContext.Deliveries.Any() 
                || !_dataContext.DeliveryStates.Any()
                || !_dataContext.DeliveryStateHistories.Any())
            {
                _dataContext.Database.EnsureDeleted();
                _dataContext.Database.Migrate();
                
                _dataContext.DeliveryStates.AddRange(Enumeration.GetAll<DeliveryState>());

                _dataContext.Deliveries.AddRange(FakeDataFactory.Deliveries);

                _dataContext.DeliveryStateHistories.AddRange(FakeDataFactory.DeliveryStateHistories);
                
                
                _dataContext.SaveChanges();
            }
        }
    }
}