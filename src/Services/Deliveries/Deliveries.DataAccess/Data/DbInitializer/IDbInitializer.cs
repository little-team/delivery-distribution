namespace Deliveries.DataAccess.Data.DbInitializer
{
    public interface IDbInitializer
    {
        public void InitializeDb();
    }
}