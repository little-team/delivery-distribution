﻿using System;
using System.Collections.Generic;
using Deliveries.Core.Domain;
using Deliveries.Core.Enumerations;

namespace Deliveries.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Delivery> Deliveries => new List<Delivery>()
        {
            new Delivery()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                CourierId = Guid.Parse("fefed0c7-53f0-4752-933a-331a389a3277"),
                CreatedOn = DateTime.Now,
                OrderId = Guid.Parse("2f36025d-50ac-4b14-a9af-b4a838e580d2"),
                DeliveryStateId = DeliveryState.Processing.Id,
                UpdatedOn = new DateTime(2021, 03, 30)
            },
            new Delivery()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                CourierId = Guid.Parse("38ece04d-8d9b-4b0d-91b2-538cd05d8300"),
                CreatedOn = DateTime.Now,
                OrderId = Guid.Parse("ab51377c-c478-4861-9e3a-5de58e5cf2e1"),
                DeliveryStateId = DeliveryState.Processing.Id,
                UpdatedOn = new DateTime(2021, 03, 31)
            }
        };

        public static IEnumerable<DeliveryStateHistory> DeliveryStateHistories => new List<DeliveryStateHistory>()
        {
            new DeliveryStateHistory()
            {
                Id = Guid.Parse("c562a747-e8bf-429c-b921-fa6fce29be35"),
                DeliveryId = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                UpdatedOn = new DateTime(2021, 03, 30),
                DeliveryStateId = DeliveryState.Processing.Id
            },
            new DeliveryStateHistory()
            {
                Id = Guid.Parse("1d1a6fda-e0eb-4140-b9d8-7e8184b9b5f9"),
                DeliveryId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                UpdatedOn = new DateTime(2021, 03, 31),
                DeliveryStateId = DeliveryState.Processing.Id
            }
        };
    }
}
