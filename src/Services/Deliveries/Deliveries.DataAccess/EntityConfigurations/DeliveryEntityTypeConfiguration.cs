﻿using Deliveries.Core.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Deliveries.DataAccess.EntityConfigurations
{
    public class DeliveryEntityTypeConfiguration
        :IEntityTypeConfiguration<Delivery>
    {
        public void Configure(EntityTypeBuilder<Delivery> builder)
        {
            builder
                .HasMany<DeliveryStateHistory>()
                .WithOne(history => history.Delivery)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}