﻿using System;

namespace Deliveries.WebHost.Dto.Delivery
{
    public class UpdateDeliveryRequest
    {
        /// <summary>
        /// Текущее состояние
        /// </summary>
        public int State { get; set; }
        
        /// <summary>
        /// Id курьера, выполняющего доставку
        /// </summary>
        public Guid CourierId { get; set; }
    }
}