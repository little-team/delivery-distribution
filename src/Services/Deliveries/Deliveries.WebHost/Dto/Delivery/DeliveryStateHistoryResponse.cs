﻿using System;

namespace Deliveries.WebHost.Dto.Delivery
{
    public record DeliveryStateHistoryResponse
    {
        public StateResponse DeliveryState { get; set; }
        
        public DateTime UpdatedOn { get; set; }
    }
}