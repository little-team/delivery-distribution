﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Deliveries.Core.Abstraction.Services;
using Deliveries.Core.Domain;
using Deliveries.WebHost.Dto.Delivery;
using Microsoft.AspNetCore.Mvc;

namespace Deliveries.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class DeliveriesController : ControllerBase
    {
        private readonly IDeliveryService _deliveryService;
        private readonly IDeliveryStateHistoryService _deliveryStateHistoryService;
        private readonly IMapper _mapper;
        public DeliveriesController(
            IDeliveryService deliveryService,
            IMapper mapper,
            IDeliveryStateHistoryService deliveryStateHistoryService)
        {
            _deliveryService = deliveryService;
            _mapper = mapper;
            _deliveryStateHistoryService = deliveryStateHistoryService;
        }


        /// <summary>
        /// Получить список всех доставок.
        /// </summary>
        /// <returns>Код завершения операции.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DeliveryShortResponse>>> GetDeliveriesAsync()
        {
            var deliveries = await _deliveryService.GetDeliveriesAsync();
            var response = _mapper.Map<IEnumerable<DeliveryShortResponse>>(deliveries);
            return Ok(response);
        }
        
        
        /// <summary>
        /// Получить данные доставки по идентификатору.
        /// </summary>
        /// <param name="id">GUID доставки.</param>
        /// <returns>Код завершения операции.</returns>
        [HttpGet("{id:guid}", Name = "GetDeliveryByIdAsync")]
        public async Task<ActionResult<GetDeliveryFullResponse>> GetDeliveryByIdAsync(Guid id)
        {
            var delivery = await _deliveryService.GetDeliveryByIdAsync(id);
            var deliveryResponse = _mapper.Map<GetDeliveryFullResponse>(delivery);
            return Ok(deliveryResponse);
        }
        
        /// <summary>
        /// Добавить новую доставку.
        /// </summary>
        /// <param name="createDelivery">Модель доставки.</param>
        /// <returns>Код завершения операции.</returns>
        [HttpPost]
        public async Task<IActionResult> AddDeliveryAsync(CreateDeliveryRequest createDelivery)
        {
            var newDelivery = _mapper.Map<Delivery>(createDelivery);
            var responseId = await _deliveryService.AddDeliveryAsync(newDelivery);
            return CreatedAtRoute(nameof(GetDeliveryByIdAsync), new { id = responseId }, responseId);
        }
        
        /// <summary>
        /// Получить все доставки одного курьера.
        /// </summary>
        /// <param name="id">GUID курьера.</param>
        /// <returns>Код завершения операции.</returns>
        [HttpGet("Courier/{id:guid}")]
         public async Task<ActionResult<IEnumerable<GetDeliveriesOfOneCourierShortResponse>>> GetCourierDeliveriesAsync(Guid id)
         {
             var deliveries = await _deliveryService.GetCourierDeliveriesAsync(id);
             var response = _mapper.Map<IEnumerable<GetDeliveriesOfOneCourierShortResponse>>(deliveries);
             return Ok(response);
         } 
        
        /// <summary>
        /// Получить все доставки одного заказа.
        /// </summary>
        /// <param name="id">GUID заказа.</param>
        /// <returns>Код завершения операции.</returns>
        [HttpGet("Order/{id:guid}")]
        public async Task<ActionResult<IEnumerable<GetDeliveriesOfOneOrderShortResponse>>> GetOrderDeliveriesAsync(Guid id)
        {
            var deliveries = await _deliveryService.GetOrderDeliveriesAsync(id);
            var response = _mapper.Map<IEnumerable<GetDeliveriesOfOneOrderShortResponse>>(deliveries);
            return Ok(response);
        }

        /// <summary>
        /// Обновить статус доставки по id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPatch("{id:guid}/state")]
        public async Task<ActionResult> UpdateDeliveryStateByIdAsync(Guid id, UpdateStateRequest request)
        {
            await _deliveryService.UpdateDeliveryStateByIdAsync(id, request.State);
            return Ok();
        }

        /// <summary>
        /// Обновить курьера, которому назначена доставка по id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPatch("{id:guid}/courier")]
        public async Task<ActionResult> UpdateDeliveryCourierByIdAsync(Guid id, UpdateCourierRequest request)
        {
            await _deliveryService.UpdateDeliveryCourierByIdAsync(id, request.CourierId);
            return Ok();
        }
        
        /// <summary>
        /// Изменить данные доставки.
        /// </summary>
        /// <param name="id">GUID доставки.</param>
        /// <param name="request">Изменяемая модель доставки.</param>
        /// <returns>Код завершения операции.</returns>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult> EditDeliveryAsync(Guid id, UpdateDeliveryRequest request)
        {
            var editedDelivery = _mapper.Map<Delivery>(request);
            await _deliveryService.EditDeliveryAsync(id, editedDelivery);
            return Ok();
        }
        
        
        /// <summary>
        /// Удалить доставку.
        /// </summary>
        /// <param name="id">GUID доставки.</param>
        /// <returns>Код завершения операции.</returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteDeliveryAsync(Guid id)
        {
            await _deliveryService.DeleteDeliveryByIdAsync(id);
            return Ok();
        }

        /// <summary>
        /// Получить историю изменений статусов для выбранной доставки
        /// </summary>
        /// <param name="id">id доставки</param>
        /// <returns></returns>
        [HttpGet("{id:guid}/history")]
        public async Task<ActionResult<IEnumerable<DeliveryStateHistoryResponse>>> GetHistoryForDeliveryByIdAsync(
            Guid id)
        {
            var historyList = await _deliveryStateHistoryService.GetHistoryForDeliveryByIdAsync(id);
            var response = _mapper.Map<IEnumerable<DeliveryStateHistoryResponse>>(historyList);
            return Ok(response);
        }
    }
}
