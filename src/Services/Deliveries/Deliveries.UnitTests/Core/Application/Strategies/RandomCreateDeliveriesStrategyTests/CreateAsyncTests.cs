﻿using System;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using Deliveries.Core.Abstraction.Services;
using Deliveries.Core.Application.Strategies;
using Deliveries.Core.Domain;
using Deliveries.UnitTests.Attributes;
using Moq;
using Xunit;
using Range = Moq.Range;

namespace Deliveries.UnitTests.Core.Application.Strategies.RandomCreateDeliveriesStrategyTests
{
    public class CreateAsyncTests
    {
        [Theory, AutoMoqData]
        public async Task CreateAsync_WeightLessThen10_AddDeliveryAsyncInvokedOnce(
            [Frozen] Mock<IDeliveryService> mockDeliveryService,
            [Frozen] RandomCreateDeliveriesStrategy sut)
        {
            //arrange
            var orderId = Guid.Parse("f5ba283f-0daf-4e0d-a6df-057c828922df");
            var weight = 1;
            
            //act
            await sut.CreateAsync(orderId, weight);
            
            //assert
            mockDeliveryService.Verify(service => service.AddDeliveryAsync(It.IsAny<Delivery>()), Times.Once);
        }

        [Theory, AutoMoqData]
        public async Task CreateAsync_WeightMoreThen10_AddDeliveryAsyncInvoked(
            [Frozen] Mock<IDeliveryService> mockDeliveryService,
            [Frozen] RandomCreateDeliveriesStrategy sut)
        {
            var orderId = Guid.Parse("f5ba283f-0daf-4e0d-a6df-057c828922df");
            var weight = 35;
            
            //act
            await sut.CreateAsync(orderId, weight);
            
            //assert
            mockDeliveryService.Verify(service => 
                service.AddDeliveryAsync(It.IsAny<Delivery>()), 
                Times.Between(1, weight/10, Range.Inclusive));
        }
    }
}