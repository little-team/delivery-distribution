﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using Deliveries.Core.Abstraction.Repositories;
using Deliveries.Core.Application.Events;
using Deliveries.Core.Application.Exceptions;
using Deliveries.Core.Application.Services;
using Deliveries.Core.Domain;
using Deliveries.Core.Enumerations;
using Deliveries.UnitTests.Attributes;
using Deliveries.UnitTests.Builders.Domain;
using FluentAssertions;
using MediatR;
using Moq;
using Xunit;

namespace Deliveries.UnitTests.Core.Application.Services.DeliveryServiceTests
{
    public class UpdateDeliveryStateByIdAsyncTests
    {
        [Theory, AutoMoqData]
        public async Task UpdateDeliveryStateByIdAsync_DeliveryNotFound_ThrowsEntityNotFoundException(
            [Frozen] Mock<IRepository<Delivery>> mockRepository,
            [Frozen] DeliveryService sut)
        {
            //arrange
            var deliveryId = DeliveryBuilder.CreateBase().Id;
            mockRepository.Setup(repo => repo.GetByIdAsync(deliveryId))
                .ReturnsAsync(default(Delivery));
            
            //act
            Func<Task> act = async () =>
                await sut.UpdateDeliveryStateByIdAsync(deliveryId, It.IsAny<int>());
            
            //assert
            await act.Should().ThrowAsync<EntityNotFoundException>();
        }

        [Theory, AutoMoqData]
        public async Task UpdateDeliveryStateByIdAsync_DeliveryExists_StateUpdated(
            [Frozen] Mock<IRepository<Delivery>> mockRepository,
            [Frozen] DeliveryService sut)
        {
            //arrange
            var delivery = DeliveryBuilder.CreateBase()
                .WithState(DeliveryState.Processing.Id);
            var newStateId = DeliveryState.InProgress.Id;

            mockRepository.Setup(repo => repo.GetByIdAsync(delivery.Id))
                .ReturnsAsync(delivery);
            
            //act
            await sut.UpdateDeliveryStateByIdAsync(delivery.Id, newStateId);
            
            //assert
            delivery.DeliveryStateId.Should().Be(newStateId);
        }
        
        [Theory, AutoMoqData]
        public async Task UpdateDeliveryStateByIdAsync_AllDeliveryHaveSameState_AllDeliveriesStatesForOrderUpdatedEventPublished(
            [Frozen] Mock<IRepository<Delivery>> mockRepository,
            [Frozen] Mock<IMediator> mockMediator,
            [Frozen] DeliveryService sut)
        {
            //arrange
            var firstDelivery = DeliveryBuilder.CreateBase()
                .WithState(1);
            var secondDelivery = DeliveryBuilder.CreateBase()
                .WithState(2);

            var deliveryList = new List<Delivery>()
            {
                firstDelivery,
                secondDelivery
            };

            mockRepository.Setup(repo => repo.GetByIdAsync(firstDelivery.Id))
                .ReturnsAsync(firstDelivery);
            mockRepository.Setup(repo => repo.GetOnConditionAsync(It.IsAny<Expression<Func<Delivery, bool>>>()))
                .ReturnsAsync(deliveryList);
            
            //act
            await sut.UpdateDeliveryStateByIdAsync(firstDelivery.Id, 2);
            
            //assert
            mockMediator.Verify(mediator => mediator.Publish(It.IsAny<AllDeliveriesStatesForOrderUpdatedEvent>(), default), Times.Once());
        }

        [Theory, AutoMoqData]
        public async Task UpdateDeliveryStateByIdAsync_DeliveriesHasDifferentStates_AllDeliveriesStatesForOrderUpdatedEventNotPublished(
            [Frozen] Mock<IRepository<Delivery>> mockRepository,
            [Frozen] Mock<IMediator> mockMediator,
            [Frozen] DeliveryService sut)
        {
            //arrange
            var firstDelivery = DeliveryBuilder.CreateBase()
                .WithState(1);
            var secondDelivery = DeliveryBuilder.CreateBase()
                .WithState(2);

            var deliveryList = new List<Delivery>()
            {
                firstDelivery,
                secondDelivery
            };

            mockRepository.Setup(repo => repo.GetByIdAsync(firstDelivery.Id))
                .ReturnsAsync(firstDelivery);
            mockRepository.Setup(repo => repo.GetOnConditionAsync(It.IsAny<Expression<Func<Delivery, bool>>>()))
                .ReturnsAsync(deliveryList);
            
            //act
            await sut.UpdateDeliveryStateByIdAsync(firstDelivery.Id, 3);
            
            //assert
            mockMediator.Verify(mediator => mediator.Publish(It.IsAny<AllDeliveriesStatesForOrderUpdatedEvent>(), default), Times.Never);
        }

        [Theory, AutoMoqData]
        public async Task UpdateDeliveryStateByIdAsync_DeliveryStateChanged_DeliveryStateUpdatedEventPublished(
            [Frozen] Mock<IRepository<Delivery>> mockRepository,
            [Frozen] Mock<IMediator> mockMediator,
            [Frozen] DeliveryService sut)
        {
            //arrange
            var delivery = DeliveryBuilder.CreateBase()
                .WithState(1);
            var newStateId = DeliveryState.InProgress.Id;

            mockRepository.Setup(repo => repo.GetByIdAsync(delivery.Id))
                .ReturnsAsync(delivery);
            
            //act
            await sut.UpdateDeliveryStateByIdAsync(delivery.Id, newStateId);
            
            //assert
            mockMediator.Verify(mediator => 
                mediator.Publish(
                    It.IsAny<DeliveryStateUpdatedEvent>(), 
                    It.IsAny<CancellationToken>()),
                times: Times.Once);
        }
        
        [Theory, AutoMoqData]
        public async Task UpdateDeliveryStateByIdAsync_DeliveryStateNotChanged_DeliveryStateUpdatedEventNotPublished(
            [Frozen] Mock<IRepository<Delivery>> mockRepository,
            [Frozen] Mock<IMediator> mockMediator,
            [Frozen] DeliveryService sut)
        {
            //arrange
            var delivery = DeliveryBuilder.CreateBase()
                .WithState(DeliveryState.Processing.Id);
            var newStateId = DeliveryState.Processing.Id;

            mockRepository.Setup(repo => repo.GetByIdAsync(delivery.Id))
                .ReturnsAsync(delivery);
            
            //act
            await sut.UpdateDeliveryStateByIdAsync(delivery.Id, newStateId);
            
            //assert
            mockMediator.Verify(mediator => 
                    mediator.Publish(
                        It.IsAny<DeliveryStateUpdatedEvent>(), 
                        It.IsAny<CancellationToken>()),
                times: Times.Never);
        }
    }
}