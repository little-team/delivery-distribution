using System;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using Deliveries.Core.Abstraction.Repositories;
using Deliveries.Core.Application.Exceptions;
using Deliveries.Core.Application.Services;
using Deliveries.Core.Domain;
using Deliveries.UnitTests.Attributes;
using Deliveries.UnitTests.Builders.Domain;
using FluentAssertions;
using Moq;
using Xunit;

namespace Deliveries.UnitTests.Core.Application.Services.DeliveryServiceTests
{
    public class DeleteDeliveryAsyncTests
    {
        [Theory, AutoMoqData]
        public async Task DeleteDeliveryByIdAsync_DeliveryNotFound_ThrowsEntityNotFoundException(
            [Frozen] Mock<IRepository<Delivery>> mockRepository,
            [Frozen] DeliveryService deliveryService)
        {
            //arrange
            var deliveryId = DeliveryBuilder.CreateBase().Id;
            mockRepository.Setup(repo => repo.GetByIdAsync(deliveryId))
                .ReturnsAsync(default(Delivery));
            
            //act
            Func<Task> act = async () =>
                await deliveryService.DeleteDeliveryByIdAsync(deliveryId);
            
            //assert
            await act.Should().ThrowAsync<EntityNotFoundException>();
        }

        [Theory, AutoMoqData]
        public async Task DeleteDeliveryByIdAsync_DeliveryExists_DeleteInvoked(
            [Frozen] Mock<IRepository<Delivery>> mockRepository,
            [Frozen] DeliveryService deliveryService)
        {
            //arrange
            var delivery = DeliveryBuilder.CreateBase();
            mockRepository.Setup(repo => repo.GetByIdAsync(delivery.Id))
                .ReturnsAsync(delivery);
            
            //act
            await deliveryService.DeleteDeliveryByIdAsync(delivery.Id);
            
            //assert
            mockRepository.Verify(repo => 
                repo.DeleteAsync(delivery), Times.Once);
        }
    }
}